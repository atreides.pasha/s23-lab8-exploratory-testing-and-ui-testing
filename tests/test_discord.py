# test_discord_search.py
import pytest
import os
from playwright.sync_api import Page

TIMEOUT = 10_000


def test_discord_login(page: Page):
    page.context.set_default_timeout(TIMEOUT)
    page.goto("https://discord.com/login")

    # Wait for the login form to load
    page.wait_for_selector('form[action="/login"]')

    # Fill in login information
    page.fill('input[name="email"]', os.getenv("DISCORD_USER"))
    page.fill('input[name="password"]', os.getenv("DISCORD_PASS"))

    # Submit the login form
    page.click('button[type="submit"]')

    # Wait for the user to be logged in
    page.wait_for_selector('div[class^="content-"]:not([style*="display: none"])')

    # Verify that the user is on the correct page after logging in
    assert page.title() == "Discord — A New Way to Chat with Friends & Communities"


def test_discord_search(page: Page):
    page.context.set_default_timeout(TIMEOUT)
    page.goto("https://discord.com")

    # Check if the search bar is present and visible
    search_bar = page.locator("[name='q'][type='text']")
    assert search_bar.is_visible()

    # Search for a server
    search_bar.fill("Python")
    page.keyboard.press("Enter")

    # Wait for the search results to load
    page.wait_for_selector("div[class^='searchResults']")

    # Check if the search results are displayed
    search_results = page.locator("div[class^='searchResults'] div[class^='result-']")
    assert search_results.is_visible()

    # Check if the first result contains the search query
    first_result = search_results.locator("h4")
    assert "Python" in first_result.text_content()


def test_discord_download_links(page: Page):
    page.context.set_default_timeout(TIMEOUT)
    # Navigate to the Discord homepage
    page.goto('https://discord.com')

    # Click the "Download" button and wait for the download page to load
    download_button = page.locator('.navButton-1YPP5U[href="/download"]')
    download_button.click()
    page.wait_for_selector('.header-3oA-Ob')

    # Verify that the page contains download links for Windows, Mac, and Linux
    windows_download_link = page.locator('.osLink-1TtTfT[title="Download for Windows"]')
    mac_download_link = page.locator('.osLink-1TtTfT[title="Download for macOS"]')
    linux_download_link = page.locator('.osLink-1TtTfT[title="Download for Linux"]')
    assert windows_download_link.is_visible()
    assert mac_download_link.is_visible()
    assert linux_download_link.is_visible()
